# Norwegian Nynorsk translation
# Copyright (C) 2021 VideoLAN
# This file is distributed under the same license as the vlc package.
#
# Translators:
# Bjørn I. <bjorn.svindseth@online.no>, 2015-2018
# Imre Kristoffer Eilertsen <imreeil42@gmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: VideoLAN's websites\n"
"Report-Msgid-Bugs-To: vlc-devel@videolan.org\n"
"POT-Creation-Date: 2020-02-05 18:13+0100\n"
"PO-Revision-Date: 2018-12-06 22:42+0100\n"
"Last-Translator: Bjørn I. <bjorn.svindseth@online.no>, 2018\n"
"Language-Team: Norwegian Nynorsk (http://www.transifex.com/yaron/vlc-trans/"
"language/nn/)\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: include/header.php:289
msgid "a project and a"
msgstr "eit prosjekt og ein"

#: include/header.php:289
msgid "non-profit organization"
msgstr "ikkje-kommersiell organisasjon"

#: include/header.php:298 include/footer.php:79
msgid "Partners"
msgstr "Partnarar"

#: include/menus.php:32
msgid "Team &amp; Organization"
msgstr "Team og organisasjon"

#: include/menus.php:33
msgid "Consulting Services &amp; Partners"
msgstr "Konsulenttenester og partnarar"

#: include/menus.php:34 include/footer.php:82
msgid "Events"
msgstr "Hendingar"

#: include/menus.php:35 include/footer.php:77 include/footer.php:111
msgid "Legal"
msgstr "Juridisk"

#: include/menus.php:36 include/footer.php:81
msgid "Press center"
msgstr "Pressesenter"

#: include/menus.php:37 include/footer.php:78
msgid "Contact us"
msgstr "Kontakt oss"

#: include/menus.php:43 include/os-specific.php:279
msgid "Download"
msgstr "Last ned"

#: include/menus.php:44 include/footer.php:35
msgid "Features"
msgstr "Funksjonar"

#: include/menus.php:45 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr "Tilpass"

#: include/menus.php:47 include/footer.php:69
msgid "Get Goodies"
msgstr "Få snadder"

#: include/menus.php:51
msgid "Projects"
msgstr "Prosjekt"

#: include/menus.php:71 include/footer.php:41
msgid "All Projects"
msgstr "Alle prosjekt"

#: include/menus.php:75 index.php:168
msgid "Contribute"
msgstr "Bidra"

#: include/menus.php:77
msgid "Getting started"
msgstr "Kom i gang"

#: include/menus.php:78 include/menus.php:96
msgid "Donate"
msgstr "Doner"

#: include/menus.php:79
msgid "Report a bug"
msgstr "Rapporter feil"

#: include/menus.php:83
msgid "Support"
msgstr "Brukarstøtte"

#: include/footer.php:33
msgid "Skins"
msgstr "Drakter"

#: include/footer.php:34
msgid "Extensions"
msgstr "Utvidingar"

#: include/footer.php:36 vlc/index.php:83
msgid "Screenshots"
msgstr "Skjermbilete"

#: include/footer.php:61
msgid "Community"
msgstr "Fellesskap"

#: include/footer.php:64
msgid "Forums"
msgstr "Forum"

#: include/footer.php:65
msgid "Mailing-Lists"
msgstr "E-postlister"

#: include/footer.php:66
msgid "FAQ"
msgstr "FAQ"

#: include/footer.php:67
msgid "Donate money"
msgstr "Doner eit beløp"

#: include/footer.php:68
msgid "Donate time"
msgstr "Doner tid"

#: include/footer.php:75
msgid "Project and Organization"
msgstr "Prosjekt og organisasjon"

#: include/footer.php:76
msgid "Team"
msgstr "Team"

#: include/footer.php:80
msgid "Mirrors"
msgstr "Speglar"

#: include/footer.php:83
msgid "Security center"
msgstr "Tryggingssenter"

#: include/footer.php:84
msgid "Get Involved"
msgstr "Ver med"

#: include/footer.php:85
msgid "News"
msgstr "Nyhende"

#: include/os-specific.php:103
msgid "Download VLC"
msgstr "Last ned VLC"

#: include/os-specific.php:109 include/os-specific.php:295 vlc/index.php:168
msgid "Other Systems"
msgstr "Andre system"

#: include/os-specific.php:260
msgid "downloads so far"
msgstr "nedlastingar så langt"

#: include/os-specific.php:648
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and "
"various streaming protocols."
msgstr ""
"VLC er ein fri og open kjeldekode, fleirplattforms-mediespelar og rammeverk "
"som spelar av dei fleste multimediefiler, samt DVD-ar, lyd-CD-ar, VCD-ar, og "
"diverse strøymeprotokollar."

#: include/os-specific.php:652
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files, and various streaming protocols."
msgstr ""
"VLC er ein fri og open kjeldekode, kryssplattforms-multimediespelar og "
"rammeverk, som spelar av dei fleste multimediefiler og ulike "
"kringkastingsprotokollar."

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr "VLC: Offisiell side - Gratis multimedialøysingar for alle OS!"

#: index.php:26
msgid "Other projects from VideoLAN"
msgstr "Andre prosjekt frå VideoLAN"

#: index.php:30
msgid "For Everyone"
msgstr "For alle"

#: index.php:40
msgid ""
"VLC is a powerful media player playing most of the media codecs and video "
"formats out there."
msgstr ""
"VLC er ein kraftig mediespelar som spelar av dei fleste mediakodekar og "
"videoformat som finst."

#: index.php:53
msgid ""
"VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr ""
"VideoLAN Movie Creator er eit ikkje-linært redigeringsprogram for video."

#: index.php:62
msgid "For Professionals"
msgstr "For professjonelle"

#: index.php:72
msgid ""
"DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr ""
"DVBlast er ein enkel og kraftfull MPEG-2/TS-demultipleks- og strøymingsapp."

#: index.php:82
msgid ""
"multicat is a set of tools designed to easily and efficiently manipulate "
"multicast streams and TS."
msgstr ""
"multicat er eit verktøysett laga for enkel og effektiv manipulasjon av "
"multicast-straumar og TS."

#: index.php:95
msgid ""
"x264 is a free application for encoding video streams into the H.264/MPEG-4 "
"AVC format."
msgstr ""
"x264 er ein gratis app for koding av videostraumar til H.264/MPEG-4 AVC-"
"formatet."

#: index.php:104
msgid "For Developers"
msgstr "For utviklarar"

#: index.php:140
msgid "View All Projects"
msgstr "Vis alle prosjekta"

#: index.php:144
msgid "Help us out!"
msgstr "Hjelp oss!"

#: index.php:148
msgid "donate"
msgstr "doner"

#: index.php:156
msgid "VideoLAN is a non-profit organization."
msgstr "VideoLAN er ein ideell organisasjon."

#: index.php:157
msgid ""
" All our costs are met by donations we receive from our users. If you enjoy "
"using a VideoLAN product, please donate to support us."
msgstr ""
"Alle kostnadane våre er dekka av donasjonar vi får frå brukarane våre. Om du "
"likar å bruka VideoLAN-produka, gjer vel med ein støttedonasjon til oss."

#: index.php:160 index.php:180 index.php:198
msgid "Learn More"
msgstr "Lær meir"

#: index.php:176
msgid "VideoLAN is open-source software."
msgstr "VideoLAN er programvare med open kjeldekode."

#: index.php:177
msgid ""
"This means that if you have the skill and the desire to improve one of our "
"products, your contributions are welcome"
msgstr ""
"Dette tyder at om du har dei rette ferdigheitene og lyst til å forbetre eitt "
"av produkta våre, er bidraga dine velkomne"

#: index.php:187
msgid "Spread the Word"
msgstr "Sprei ordet"

#: index.php:195
msgid ""
"We feel that VideoLAN has the best video software available at the best "
"price: free. If you agree please help spread the word about our software."
msgstr ""
"Me synest at VideoLAN har den beste programvara tilgjengeleg, til den beste "
"prisen: Gratis. Om du er samd kan du gjerne hjelpa oss med å spreia ordet om "
"programvara vår."

#: index.php:215
msgid "News &amp; Updates"
msgstr "Nyhende og oppdateringar"

#: index.php:218
msgid "More News"
msgstr "Fleire nyhende"

#: index.php:222
msgid "Development Blogs"
msgstr "Utviklingsbloggar"

#: index.php:251
msgid "Social media"
msgstr "Sosiale media"

#: vlc/index.php:3
msgid "Official download of VLC media player, the best Open Source player"
msgstr "Offisiell nedlasting av VLC mediespelar, den beste frie mediaspelaren"

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "Last ned VLC for"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr "Enkel, rask og kraftig"

#: vlc/index.php:35
msgid "Plays everything"
msgstr "Spelar alt"

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr "Filer, plater, nettkamera, einingar og straumar."

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr "Spelar dei fleste kodekar utan at ein treng ekstra kodekpakkar"

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr "Køyrer på alle plattformer"

#: vlc/index.php:44
msgid "Completely Free"
msgstr "Heilt gratis"

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr "ikkje noko spionvare, reklame eller brukarsporing"

#: vlc/index.php:47
msgid "learn more"
msgstr "les meir"

#: vlc/index.php:66
msgid "Add"
msgstr "Legg til"

#: vlc/index.php:66
msgid "skins"
msgstr "drakter"

#: vlc/index.php:69
msgid "Create skins with"
msgstr "Lag drakter med"

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr "VLC draktredigerar"

#: vlc/index.php:72
msgid "Install"
msgstr "Installer"

#: vlc/index.php:72
msgid "extensions"
msgstr "utvidingar"

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "Vis alle skjermbilda"

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "Offfisielle nedlastingar av VLC media player"

#: vlc/index.php:146
msgid "Sources"
msgstr "Kjelder"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "Du kan òg direkte få"

#: vlc/index.php:148
msgid "source code"
msgstr "kjeldekoden"
